<img src="main_data_dir/image.jpg" width="50%" align="middle" alt="None">  

# **16S/ITS METABARCODING OF MICROBIAL COMMUNITIES**  

## ***Rationale***
This course will provide a thorough introduction to the application of metabarcoding techniques in microbial ecology.  
The topics covered by the course range from *bioinformatic* processing of next-generation sequencing data to the most important approaches in multivariate statistics. Using a combination of theoretical lectures and hands-on exercises, the participants will learn the most important computational steps of a metabarcoding study from the processing of raw sequencing reads down to the final statistical evaluations. After completing the course, the participants should be able to understand the potential and limitations of metabarcoding techniques as well as to process their own datasets to answer the questions under investigation.  
__FORMAT__  
This course is designed for researchers and students with strong interests in applying novel high-throughput DNA sequencing technologies to answer questions in the area of community ecology and biodiversity.  
The course will mainly focus on the analysis of phylogenetic markers to study bacterial, archaeal and fungal assemblages in the environment, but the theoretical concepts and computational procedures can be equally applied to any taxonomic group or gene of interest.  
__ASSUMED BACKGROUND__  
The participants should have some basic background in biology and understand the central role of DNA for biodiversity studies. No programming or scripting expertise is required and some basic introduction to UNIX-based command line applications will be provided on the first day. However, some basic experience with using command line and/or R is clearly an advantage as not all the basics can be thoroughly covered in that short amount of time.  
All the hands-on exercises will be carried out using [**QIIME2**](https://qiime2.org/) platform . No previous knowledge of computer science is required but a basic knowledge of “bash” would allow to focus more on the microbial analysis.  
__LEARNING OUTCOMES__  
1) Understanding the concept, potential and limitation of microbial metabarcoding techniques.
2) Learning how to process raw sequencing reads to obtain meaningful information.
3) Obtaining experience on how to statistically evaluate and visualize your data.
4) Being able to make informed decisions on best practices for your own data.  

:exclamation: :exclamation: :exclamation: **All the papers discussed during the course are available in this [Google Drive Folder](https://drive.google.com/open?id=1FLkzwWiBYlIG2FrpceDrgL-ru_A8ZO6_)** :exclamation: :exclamation: :exclamation:  


## Program
[Physalia Welcome](https://drive.google.com/open?id=1zAqld5-NcofYez4QYsGGvX0ZtJGphRNQHCBVwUgYNVE)
## Day 1
### Morning
- [X] [Introduction to the course idea and Instructors](https://drive.google.com/open?id=1oLH_W-5IMYIHZoe9Kr7kPdYyK4PaAy0r3YMB_7d5nEY):  
    - [Xavier Harrison](https://scholar.google.co.uk/citations?user=-LKz8tIAAAAJ&hl=en)  
    - [Daniel Anotny Pass](https://scholar.google.com/citations?user=XQml0DQAAAAJ&hl=en)  

- [X] [**Brief introduction** about __Metagenomics__ concepts. *What are we going to talk about?*](https://docs.google.com/presentation/d/1JafDY-zalbqZkEA83MLhLHNrCJZ_aqDlfzt22FNKadw/edit?usp=sharing)
- [X] [**Illumina sequencing technology** and how the sequences are obtained;](https://drive.google.com/open?id=1duo88-s5jdFg_dPnaXAsa6-c9YpeSpYhVbpRw1K0M3g)
- [X] [How to connect to the Amazon EC2 service](unix_short_tutorial/how_to_connect.md)  
- [X] [A brief introduction about **Anaconda** and the **BASH** environment;](https://docs.google.com/presentation/d/1OlNpcup5JsqLR0-DX9q0tG-GFBEIvNgDCKD3BVSfung/edit?usp=sharing)


### Afternoon
- [X] Laptop setting;
- [X] [Introduction to the **BASH** shell and **FASTQ** format](unix_short_tutorial/Readme.md)
- [X] [Introduction to the **QIIME2** objects, Importing and visualizing raw data](unix_short_tutorial/Readme.md)
- [X] [Experimental Design](https://drive.google.com/open?id=1tD3j6U2lBpnC87F9L4lgtcHxOa-cpfTNM1L6s75YwKA)
- [X] [Atacama Soil Experiment and Loading data](16S_ITS_tutorial/readme.md)


## Day 2
### Morning

- [X] [Lets review](unix_short_tutorial/Readme.md)
- [X] [**Key concept: OTU or ASV**;](https://docs.google.com/presentation/d/1X8s-J-gOjWvXYDAnRh39_nwKuLJtU0l0fG2MrLXyUpo/edit?usp=sharing)
- [X] [ Denoising and **DADA2**](16S_ITS_tutorial/readme.md);

### Afternoon

- [X] [Taxonomic assignment](https://drive.google.com/open?id=1oHTCBiJ1HoHAREZIN2NVSHnC63QphDUJr_cPbgqgDs4)
- [X] [Taxonomy assignment: **VSEARCH** vs **classify-sklearn**](16S_ITS_tutorial/readme.md#step-3-summarizing-feature-table-and-feature-data);
- [X] [ITS group](ITS/readme.md)

## Day 3
### Morning

- [X] [Diversity Analysis](https://drive.google.com/open?id=1aT_z18Iv8X1V0pv--9GqA1kacQl4NSWWH0_Z0LkBgRE)
    * Download the PDF version by using this [LINK](https://www.dropbox.com/s/9oajzmhg98bksgd/Diversity%20Analysis.pdf?dl=0)
- [ ] [Alpha and Beta Diversity in QIIME2](16S_ITS_tutorial/readme.md#step-6-analyzing-alpha-and-beta-diversities)


### Afternoon
- [ ] [R tutorial](https://www.datacamp.com/courses/free-introduction-to-r)
 
## Day 4
### Morning
- [ ] Lecture  - Multivariate Analysis of Ecological Communities  
- [ ] Traits of Alpha and Beta Diversity (richness, evenness, dispersion)  
- [ ] Multivariate Tests for differences in microbial community composition  

### Afternoon
- [ ] Lab  - Multivariate Statistics  
    - [ ] Data import & preparation (normalisations, transformations, metadata)  
    - [ ] Alpha Diversity (indices of diversity, rarefaction curves)  
    - [ ] Heatmaps to visualise microbial community differences  
    - [ ] Unconstrained and Constrained Ordination (PCoA, NMDS, CCA, DCA)  
    - [ ] Multivariate tests for differences in community composition (PERMANOVA, PERMDISP)  
    - [ ] Taxon-level responses (DESeq2)

## Day 5
### Morning  
- [ ] Lecture - GLMs and Mixed Models for Microbiome Data  
    - [ ] Using Traits of Microbiome structure in GLMs and Mixed Models  
    - [ ] Model selection for GLMs and (G)LMMs  
    - [ ] Combining Microbiome data and life history data  

- [ ] Lab - Mixed Models   
    - [ ] Fitting GLMs and (G)LMMs in R
    - [ ] Model Selection and presentation of results  
    - [ ] Plotting effects

